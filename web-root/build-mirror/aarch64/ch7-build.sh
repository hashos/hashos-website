#!/bin/bash
#
# PiLFS Build Script SVN-20201203 v1.0
# Builds chapters 7.7 Libstdc++ from GCC - Pass 2 to 8.75 - Sysvinit
# https://intestinate.com/pilfs
#
# Optional parameteres below:

RPI_MODEL=4                     # Which Raspberry Pi model are you building for - this selects the right GCC CPU patch.
PARALLEL_JOBS=4                 # Number of parallel make jobs, 1 for RPi1 and 4 for RPi2 and up recommended.
LOCAL_TIMEZONE=Europe/London    # Use this timezone from /usr/share/zoneinfo/ to set /etc/localtime. See "8.8.2 Configuring Glibc".
GROFF_PAPER_SIZE=A4             # Use this default paper size for Groff. See "8.57 Groff".
INSTALL_OPTIONAL_DOCS=1         # Install optional documentation when given a choice?
INSTALL_ALL_LOCALES=0           # Install all glibc locales? By default only en_US.ISO-8859-1 and en_US.UTF-8 are installed.

# End of optional parameters

set -o nounset
set -o errexit

function prebuild_sanity_check {
    if [[ $(whoami) != "root" ]] ; then
        echo "You should be running as root for chapter 7!"
        exit 1
    fi

    if ! [[ -d /sources ]] ; then
        echo "Can't find your sources directory! Did you forget to chroot?"
        exit 1
    fi

    if ! [[ -d /tools ]] ; then
        echo "Can't find your tools directory! Did you forget to chroot?"
        exit 1
    fi
}

function check_tarballs {
LIST_OF_TARBALLS="
gcc-10.2.0.tar.xz
gcc-9.1.0-rpi1-cpu-default.patch
gcc-9.1.0-rpi2-cpu-default.patch
gcc-9.1.0-rpi3-cpu-default.patch
gcc-9.1.0-rpi4-cpu-default.patch
bison-3.7.4.tar.xz
gettext-0.21.tar.xz
perl-5.32.0.tar.xz
Python-3.9.0.tar.xz
python-3.9.0-docs-html.tar.bz2
texinfo-6.7.tar.xz
util-linux-2.36.1.tar.xz
man-pages-5.09.tar.xz
tcl8.6.10-src.tar.gz
tcl8.6.10-html.tar.gz
expect5.45.4.tar.gz
dejagnu-1.6.2.tar.gz
iana-etc-20201124.tar.gz
glibc-2.32.tar.xz
glibc-2.32-fhs-1.patch
tzdata2020d.tar.gz
zlib-1.2.11.tar.xz
bzip2-1.0.8.tar.gz
bzip2-1.0.8-install_docs-1.patch
xz-5.2.5.tar.xz
zstd-1.4.5.tar.gz
file-5.39.tar.gz
readline-8.0.tar.gz
m4-1.4.18.tar.xz
bc-3.2.3.tar.xz
flex-2.6.4.tar.gz
binutils-2.35.1.tar.xz
gmp-6.2.1.tar.xz
mpfr-4.1.0.tar.xz
mpc-1.2.1.tar.gz
attr-2.4.48.tar.gz
acl-2.2.53.tar.gz
libcap-2.45.tar.xz
shadow-4.8.1.tar.xz
pkg-config-0.29.2.tar.gz
ncurses-6.2.tar.gz
sed-4.8.tar.xz
psmisc-23.3.tar.xz
grep-3.6.tar.xz
bash-5.0.tar.gz
bash-5.0-upstream_fixes-1.patch
libtool-2.4.6.tar.xz
gdbm-1.18.1.tar.gz
gperf-3.1.tar.gz
expat-2.2.10.tar.xz
inetutils-1.9.4.tar.xz
XML-Parser-2.46.tar.gz
intltool-0.51.0.tar.gz
autoconf-2.69-consolidated_fixes-1.patch
autoconf-2.69.tar.xz
automake-1.16.3.tar.xz
kmod-27.tar.xz
elfutils-0.182.tar.bz2
libffi-3.3.tar.gz
openssl-1.1.1h.tar.gz
ninja-1.10.2.tar.gz
meson-0.56.0.tar.gz
coreutils-8.32.tar.xz
coreutils-8.32-i18n-1.patch
check-0.15.2.tar.gz
diffutils-3.7.tar.xz
gawk-5.1.0.tar.xz
findutils-4.7.0.tar.xz
groff-1.22.4.tar.gz
less-563.tar.gz
gzip-1.10.tar.xz
iproute2-5.9.0.tar.xz
kbd-2.3.0.tar.xz
kbd-2.3.0-backspace-1.patch
libpipeline-1.5.3.tar.gz
make-4.3.tar.gz
patch-2.7.6.tar.xz
man-db-2.9.3.tar.xz
tar-1.32.tar.xz
vim-8.2.2070.tar.gz
eudev-3.2.9.tar.gz
udev-lfs-20171102.tar.xz
procps-ng-3.3.16.tar.xz
e2fsprogs-1.45.6.tar.gz
sysklogd-1.5.1.tar.gz
sysvinit-2.98.tar.xz
sysvinit-2.98-consolidated-1.patch
master.tar.gz
v2020.09.03-138a1.tar.gz
"

for tarball in $LIST_OF_TARBALLS ; do
    if ! [[ -f /sources/$tarball ]] ; then
        echo "Can't find /sources/$tarball!"
        exit 1
    fi
done
}

function timer {
    if [[ $# -eq 0 ]]; then
        echo $(date '+%s')
    else
        local stime=$1
        etime=$(date '+%s')
        if [[ -z "$stime" ]]; then stime=$etime; fi
        dt=$((etime - stime))
        ds=$((dt % 60))
        dm=$(((dt / 60) % 60))
        dh=$((dt / 3600))
        printf '%02d:%02d:%02d' $dh $dm $ds
    fi
}

prebuild_sanity_check
check_tarballs

if [[ $(cat /proc/swaps | wc -l) == 1 ]] ; then
    echo -e "\nYou are almost certainly going to want to add some swap space before building!"
    echo -e "(See https://intestinate.com/pilfs/beyond.html#addswap for instructions)"
    echo -e "Continue without swap?"
    select yn in "Yes" "No"; do
        case $yn in
            Yes) break;;
            No) exit;;
        esac
    done
fi

echo -e "\nThis is your last chance to quit before we start building... continue?"
echo "(Note that if anything goes wrong during the build, the script will abort mission)"
select yn in "Yes" "No"; do
    case $yn in
        Yes) break;;
        No) exit;;
    esac
done

total_time=$(timer)

echo "# 7.7. Libstdc++ from GCC-10.2.0, Pass 2"
tar -Jxf gcc-10.2.0.tar.xz
cd gcc-10.2.0
ln -s gthr-posix.h libgcc/gthr-default.h
mkdir -v build
cd build
../libstdc++-v3/configure            \
    CXXFLAGS="-g -O2 -D_GNU_SOURCE"  \
    --prefix=/usr                    \
    --disable-multilib               \
    --disable-nls                    \
    --host=$(uname -m)-lfs-linux-gnueabihf \
    --disable-libstdcxx-pch
make -j $PARALLEL_JOBS
make install
cd /sources
rm -rf gcc-10.2.0

echo "# 7.8. Gettext-0.21"
tar -Jxf gettext-0.21.tar.xz
cd gettext-0.21
./configure --disable-shared
make -j $PARALLEL_JOBS
cp -v gettext-tools/src/{msgfmt,msgmerge,xgettext} /usr/bin
cd /sources
rm -rf gettext-0.21

echo "# 7.9. Bison-3.7.4"
tar -Jxf bison-3.7.4.tar.xz
cd bison-3.7.4
./configure --prefix=/usr \
            --docdir=/usr/share/doc/bison-3.7.4
make -j $PARALLEL_JOBS
make install
cd /sources
rm -rf bison-3.7.4

echo "# 7.10. Perl-5.32.0"
tar -Jxf perl-5.32.0.tar.xz
cd perl-5.32.0
sh Configure -des                                        \
             -Dprefix=/usr                               \
             -Dvendorprefix=/usr                         \
             -Dprivlib=/usr/lib/perl5/5.32/core_perl     \
             -Darchlib=/usr/lib/perl5/5.32/core_perl     \
             -Dsitelib=/usr/lib/perl5/5.32/site_perl     \
             -Dsitearch=/usr/lib/perl5/5.32/site_perl    \
             -Dvendorlib=/usr/lib/perl5/5.32/vendor_perl \
             -Dvendorarch=/usr/lib/perl5/5.32/vendor_perl
make -j $PARALLEL_JOBS
make install
cd /sources
rm -rf perl-5.32.0

echo "# 7.11. Python-3.9.0"
tar -Jxf Python-3.9.0.tar.xz
cd Python-3.9.0
./configure --prefix=/usr   \
            --enable-shared \
            --without-ensurepip
make -j $PARALLEL_JOBS
make install 
cd /sources
rm -rf Python-3.9.0

echo "# 7.12. Texinfo-6.7"
tar -Jxf texinfo-6.7.tar.xz
cd texinfo-6.7
./configure --prefix=/usr
make -j $PARALLEL_JOBS
make install
cd /sources
rm -rf texinfo-6.7

echo "# 7.13. Util-linux-2.36.1"
tar -Jxf util-linux-2.36.1.tar.xz
cd util-linux-2.36.1
mkdir -pv /var/lib/hwclock
./configure ADJTIME_PATH=/var/lib/hwclock/adjtime    \
            --docdir=/usr/share/doc/util-linux-2.36.1 \
            --disable-chfn-chsh  \
            --disable-login      \
            --disable-nologin    \
            --disable-su         \
            --disable-setpriv    \
            --disable-runuser    \
            --disable-pylibmount \
            --disable-static     \
            --without-python
make -j $PARALLEL_JOBS
make install
cd /sources
rm -rf util-linux-2.36.1

echo "# 7.14. Cleaning up and Saving the Temporary System"
find /usr/{lib,libexec} -name \*.la -delete
rm -rf /usr/share/{info,man,doc}/*
# In order to keep our script running we will not exit the chroot environment here

echo "# 8.3. Man-pages-5.09"
tar -Jxf man-pages-5.09.tar.xz
cd man-pages-5.09
make install
cd /sources
rm -rf man-pages-5.09

echo "# 8.4. Tcl-8.6.10"
tar -zxf tcl8.6.10-src.tar.gz
cd tcl8.6.10
tar -xf ../tcl8.6.10-html.tar.gz --strip-components=1
SRCDIR=$(pwd)
cd unix
./configure --prefix=/usr           \
            --mandir=/usr/share/man
make -j $PARALLEL_JOBS
sed -e "s|$SRCDIR/unix|/usr/lib|" \
    -e "s|$SRCDIR|/usr/include|"  \
    -i tclConfig.sh
sed -e "s|$SRCDIR/unix/pkgs/tdbc1.1.1|/usr/lib/tdbc1.1.1|" \
    -e "s|$SRCDIR/pkgs/tdbc1.1.1/generic|/usr/include|"    \
    -e "s|$SRCDIR/pkgs/tdbc1.1.1/library|/usr/lib/tcl8.6|" \
    -e "s|$SRCDIR/pkgs/tdbc1.1.1|/usr/include|"            \
    -i pkgs/tdbc1.1.1/tdbcConfig.sh
sed -e "s|$SRCDIR/unix/pkgs/itcl4.2.0|/usr/lib/itcl4.2.0|" \
    -e "s|$SRCDIR/pkgs/itcl4.2.0/generic|/usr/include|"    \
    -e "s|$SRCDIR/pkgs/itcl4.2.0|/usr/include|"            \
    -i pkgs/itcl4.2.0/itclConfig.sh
unset SRCDIR
make install
chmod -v u+w /usr/lib/libtcl8.6.so
make install-private-headers
ln -sfv tclsh8.6 /usr/bin/tclsh
mv /usr/share/man/man3/{Thread,Tcl_Thread}.3
cd /sources
rm -rf tcl8.6.10

echo "# 8.5. Expect-5.45.4"
tar -zxf expect5.45.4.tar.gz
cd expect5.45.4
./configure --prefix=/usr           \
            --with-tcl=/usr/lib     \
            --enable-shared         \
            --mandir=/usr/share/man \
            --with-tclinclude=/usr/include
make -j $PARALLEL_JOBS
make install
ln -svf expect5.45.4/libexpect5.45.4.so /usr/lib
cd /sources
rm -rf expect5.45.4

echo "# 8.6. DejaGNU-1.6.2"
tar -zxf dejagnu-1.6.2.tar.gz
cd dejagnu-1.6.2
./configure --prefix=/usr
makeinfo --html --no-split -o doc/dejagnu.html doc/dejagnu.texi
makeinfo --plaintext       -o doc/dejagnu.txt  doc/dejagnu.texi
make install
install -v -dm755 /usr/share/doc/dejagnu-1.6.2
install -v -m644 doc/dejagnu.{html,txt} /usr/share/doc/dejagnu-1.6.2
cd /sources
rm -rf dejagnu-1.6.2

echo "# 8.7. Iana-Etc-20201124"
tar -zxf iana-etc-20201124.tar.gz
cd iana-etc-20201124
cp services protocols /etc
cd /sources
rm -rf iana-etc-20201124

echo "# 8.8. Glibc-2.32"
tar -Jxf glibc-2.32.tar.xz
cd glibc-2.32
patch -Np1 -i ../glibc-2.32-fhs-1.patch
mkdir -v build
cd build
../configure --prefix=/usr                            \
             --disable-werror                         \
             --enable-kernel=3.2                      \
             --enable-stack-protector=strong          \
             --with-headers=/usr/include              \
             libc_cv_slibdir=/lib
make -j $PARALLEL_JOBS
touch /etc/ld.so.conf
sed '/test-installation/s@$(PERL)@echo not running@' -i ../Makefile
make install
cp -v ../nscd/nscd.conf /etc/nscd.conf
mkdir -pv /var/cache/nscd
if [[ $INSTALL_ALL_LOCALES = 1 ]] ; then
    make localedata/install-locales
else
    mkdir -pv /usr/lib/locale
    localedef -i en_US -f ISO-8859-1 en_US
    localedef -i en_US -f UTF-8 en_US.UTF-8
fi
cat > /etc/nsswitch.conf << "EOF"
# Begin /etc/nsswitch.conf

passwd: files
group: files
shadow: files

hosts: files dns
networks: files

protocols: files
services: files
ethers: files
rpc: files

# End /etc/nsswitch.conf
EOF
tar -zxf ../../tzdata2020d.tar.gz
ZONEINFO=/usr/share/zoneinfo
mkdir -pv $ZONEINFO/{posix,right}
for tz in etcetera southamerica northamerica europe africa antarctica  \
          asia australasia backward; do
    zic -L /dev/null   -d $ZONEINFO       ${tz}
    zic -L /dev/null   -d $ZONEINFO/posix ${tz}
    zic -L leapseconds -d $ZONEINFO/right ${tz}
done
cp -v zone.tab zone1970.tab iso3166.tab $ZONEINFO
zic -d $ZONEINFO -p America/New_York
unset ZONEINFO
if ! [[ -f /usr/share/zoneinfo/$LOCAL_TIMEZONE ]] ; then
    echo "Seems like your timezone won't work out. Defaulting to London. Either fix it yourself later or consider moving there :)"
    ln -sfv /usr/share/zoneinfo/Europe/London /etc/localtime
else
    ln -sfv /usr/share/zoneinfo/$LOCAL_TIMEZONE /etc/localtime
fi
cat > /etc/ld.so.conf << "EOF"
# Begin /etc/ld.so.conf
/usr/local/lib

EOF
cat >> /etc/ld.so.conf << "EOF"
# Add an include directory
include /etc/ld.so.conf.d/*.conf

EOF
mkdir -pv /etc/ld.so.conf.d
# Compatibility symlink for non ld-linux-armhf awareness
ln -sv ld-2.32.so /lib/ld-linux.so.3
cd /sources
rm -rf glibc-2.32

echo "# 8.9. Zlib-1.2.11"
tar -Jxf zlib-1.2.11.tar.xz
cd zlib-1.2.11
./configure --prefix=/usr
make -j $PARALLEL_JOBS
make install
mv -v /usr/lib/libz.so.* /lib
ln -sfv ../../lib/$(readlink /usr/lib/libz.so) /usr/lib/libz.so
cd /sources
rm -rf zlib-1.2.11

echo "# 8.10. Bzip2-1.0.8"
tar -zxf bzip2-1.0.8.tar.gz
cd bzip2-1.0.8
patch -Np1 -i ../bzip2-1.0.8-install_docs-1.patch
sed -i 's@\(ln -s -f \)$(PREFIX)/bin/@\1@' Makefile
sed -i "s@(PREFIX)/man@(PREFIX)/share/man@g" Makefile
make -j $PARALLEL_JOBS -f Makefile-libbz2_so
make clean
make -j $PARALLEL_JOBS
make PREFIX=/usr install
cp -v bzip2-shared /bin/bzip2
cp -av libbz2.so* /lib
ln -sv ../../lib/libbz2.so.1.0 /usr/lib/libbz2.so
rm -v /usr/bin/{bunzip2,bzcat,bzip2}
ln -sv bzip2 /bin/bunzip2
ln -sv bzip2 /bin/bzcat
cd /sources
rm -rf bzip2-1.0.8

echo "# 8.11. Xz-5.2.5"
tar -Jxf xz-5.2.5.tar.xz
cd xz-5.2.5
./configure --prefix=/usr    \
            --disable-static \
            --docdir=/usr/share/doc/xz-5.2.5
make -j $PARALLEL_JOBS
make install
mv -v /usr/bin/{lzma,unlzma,lzcat,xz,unxz,xzcat} /bin
mv -v /usr/lib/liblzma.so.* /lib
ln -svf ../../lib/$(readlink /usr/lib/liblzma.so) /usr/lib/liblzma.so
cd /sources
rm -rf xz-5.2.5

echo "# 8.12. Zstd-1.4.5"
tar -zxf zstd-1.4.5.tar.gz
cd zstd-1.4.5
make -j $PARALLEL_JOBS
make prefix=/usr install
rm -v /usr/lib/libzstd.a
mv -v /usr/lib/libzstd.so.* /lib
ln -sfv ../../lib/$(readlink /usr/lib/libzstd.so) /usr/lib/libzstd.so
cd /sources
rm -rf zstd-1.4.5

echo "# 8.13. File-5.39"
tar -zxf file-5.39.tar.gz
cd file-5.39
./configure --prefix=/usr
make -j $PARALLEL_JOBS
make install
cd /sources
rm -rf file-5.39

echo "# 8.14. Readline-8.0"
tar -zxf readline-8.0.tar.gz
cd readline-8.0
sed -i '/MV.*old/d' Makefile.in
sed -i '/{OLDSUFF}/c:' support/shlib-install
./configure --prefix=/usr    \
            --disable-static \
            --with-curses    \
            --docdir=/usr/share/doc/readline-8.0
make -j $PARALLEL_JOBS SHLIB_LIBS="-lncursesw"
make SHLIB_LIBS="-lncursesw" install
mv -v /usr/lib/lib{readline,history}.so.* /lib
chmod -v u+w /lib/lib{readline,history}.so.*
ln -sfv ../../lib/$(readlink /usr/lib/libreadline.so) /usr/lib/libreadline.so
ln -sfv ../../lib/$(readlink /usr/lib/libhistory.so ) /usr/lib/libhistory.so
if [[ $INSTALL_OPTIONAL_DOCS = 1 ]] ; then
    install -v -m644 doc/*.{ps,pdf,html,dvi} /usr/share/doc/readline-8.0
fi
cd /sources
rm -rf readline-8.0

echo "# 8.15. M4-1.4.18"
tar -Jxf m4-1.4.18.tar.xz
cd m4-1.4.18
sed -i 's/IO_ftrylockfile/IO_EOF_SEEN/' lib/*.c
echo "#define _IO_IN_BACKUP 0x100" >> lib/stdio-impl.h
./configure --prefix=/usr
make -j $PARALLEL_JOBS
make install
cd /sources
rm -rf m4-1.4.18

echo "# 8.16. Bc-3.2.3"
tar -Jxf bc-3.2.3.tar.xz
cd bc-3.2.3
PREFIX=/usr CC=gcc CFLAGS="-std=c99" ./configure.sh -G -O3
make -j $PARALLEL_JOBS
make install
cd /sources
rm -rf bc-3.2.3

echo "# 8.17. Flex-2.6.4"
tar -zxf flex-2.6.4.tar.gz
cd flex-2.6.4
./configure --prefix=/usr --docdir=/usr/share/doc/flex-2.6.4
make -j $PARALLEL_JOBS
make install
ln -sv flex /usr/bin/lex
cd /sources
rm -rf flex-2.6.4

echo "# 8.18. Binutils-2.35.1"
tar -Jxf binutils-2.35.1.tar.xz
cd binutils-2.35.1
mkdir -v build
cd build
../configure --prefix=/usr       \
             --enable-gold       \
             --enable-ld=default \
             --enable-plugins    \
             --enable-shared     \
             --disable-werror    \
             --enable-64-bit-bfd \
             --with-system-zlib
make -j $PARALLEL_JOBS tooldir=/usr
make tooldir=/usr install
cd /sources
rm -rf binutils-2.35.1

echo "# 8.19. GMP-6.2.1"
tar -Jxf gmp-6.2.1.tar.xz
cd gmp-6.2.1
./configure --prefix=/usr    \
            --enable-cxx     \
            --disable-static \
            --docdir=/usr/share/doc/gmp-6.2.1
make -j $PARALLEL_JOBS
make install
if [[ $INSTALL_OPTIONAL_DOCS = 1 ]] ; then
    make html
    make install-html
fi
cd /sources
rm -rf gmp-6.2.1

echo "# 8.20. MPFR-4.1.0"
tar -Jxf mpfr-4.1.0.tar.xz
cd mpfr-4.1.0
./configure  --prefix=/usr        \
             --disable-static     \
             --enable-thread-safe \
             --docdir=/usr/share/doc/mpfr-4.1.0
make -j $PARALLEL_JOBS
make install
if [[ $INSTALL_OPTIONAL_DOCS = 1 ]] ; then
    make html
    make install-html
fi
cd /sources
rm -rf mpfr-4.1.0

echo "# 8.21. MPC-1.2.1"
tar -zxf mpc-1.2.1.tar.gz
cd mpc-1.2.1
./configure --prefix=/usr    \
            --disable-static \
            --docdir=/usr/share/doc/mpc-1.2.1
make -j $PARALLEL_JOBS
make install
if [[ $INSTALL_OPTIONAL_DOCS = 1 ]] ; then
    make html
    make install-html
fi
cd /sources
rm -rf mpc-1.2.1

echo "# 8.22. Attr-2.4.48"
tar -zxf attr-2.4.48.tar.gz
cd attr-2.4.48
./configure --prefix=/usr     \
            --bindir=/bin     \
            --disable-static  \
            --sysconfdir=/etc \
            --docdir=/usr/share/doc/attr-2.4.48
make -j $PARALLEL_JOBS
make install
mv -v /usr/lib/libattr.so.* /lib
ln -sfv ../../lib/$(readlink /usr/lib/libattr.so) /usr/lib/libattr.so
cd /sources
rm -rf attr-2.4.48

echo "# 8.23. Acl-2.2.53"
tar -zxf acl-2.2.53.tar.gz
cd acl-2.2.53
./configure --prefix=/usr         \
            --bindir=/bin         \
            --disable-static      \
            --libexecdir=/usr/lib \
            --docdir=/usr/share/doc/acl-2.2.53
make -j $PARALLEL_JOBS
make install
mv -v /usr/lib/libacl.so.* /lib
ln -sfv ../../lib/$(readlink /usr/lib/libacl.so) /usr/lib/libacl.so
cd /sources
rm -rf acl-2.2.53

echo "# 8.24. Libcap-2.45"
tar -Jxf libcap-2.45.tar.xz
cd libcap-2.45
sed -i '/install -m.*STA/d' libcap/Makefile
make -j $PARALLEL_JOBS lib=lib
make lib=lib PKGCONFIGDIR=/usr/lib/pkgconfig install
for libname in cap psx; do
    chmod -v 755 /lib/lib${libname}.so.2.45
    rm -v /lib/lib${libname}.so
    ln -sfv ../../lib/lib${libname}.so.2 /usr/lib/lib${libname}.so
done
cd /sources
rm -rf libcap-2.45

echo "# 8.25. Shadow-4.8.1"
tar -Jxf shadow-4.8.1.tar.xz
cd shadow-4.8.1
sed -i 's/groups$(EXEEXT) //' src/Makefile.in
find man -name Makefile.in -exec sed -i 's/groups\.1 / /'   {} \;
find man -name Makefile.in -exec sed -i 's/getspnam\.3 / /' {} \;
find man -name Makefile.in -exec sed -i 's/passwd\.5 / /'   {} \;
sed -e 's:#ENCRYPT_METHOD DES:ENCRYPT_METHOD SHA512:' \
    -e 's:/var/spool/mail:/var/mail:'                 \
    -i etc/login.defs
sed -i 's/1000/999/' etc/useradd
touch /usr/bin/passwd
./configure --sysconfdir=/etc \
            --with-group-name-max-length=32
make -j $PARALLEL_JOBS
make install
pwconv
grpconv
sed -i 's/yes/no/' /etc/default/useradd
# passwd root
# Root password will be set at the end of the script to prevent a stop here
cd /sources
rm -rf shadow-4.8.1

echo "# 8.26. GCC-10.2.0"
tar -Jxf gcc-10.2.0.tar.xz
cd gcc-10.2.0
patch -Np1 -i ../gcc-9.1.0-rpi$RPI_MODEL-cpu-default.patch
mkdir -v build
cd build
../configure --prefix=/usr            \
             LD=ld                    \
             --enable-languages=c,c++ \
             --disable-multilib       \
             --disable-bootstrap      \
             --with-system-zlib
make -j $PARALLEL_JOBS
make install
rm -rf /usr/lib/gcc/$(gcc -dumpmachine)/10.2.0/include-fixed/bits/
chown -v -R root:root /usr/lib/gcc/*linux-gnu*/10.2.0/include{,-fixed}
ln -sv ../usr/bin/cpp /lib
install -v -dm755 /usr/lib/bfd-plugins
ln -sfv ../../libexec/gcc/$(gcc -dumpmachine)/10.2.0/liblto_plugin.so /usr/lib/bfd-plugins/
mkdir -pv /usr/share/gdb/auto-load/usr/lib
mv -v /usr/lib/*gdb.py /usr/share/gdb/auto-load/usr/lib
cd /sources
rm -rf gcc-10.2.0

echo "# 8.27. Pkg-config-0.29.2"
tar -zxf pkg-config-0.29.2.tar.gz
cd pkg-config-0.29.2
./configure --prefix=/usr              \
            --with-internal-glib       \
            --disable-host-tool        \
            --docdir=/usr/share/doc/pkg-config-0.29.2
make -j $PARALLEL_JOBS
make install
cd /sources
rm -rf pkg-config-0.29.2

echo "# 8.28. Ncurses-6.2"
tar -zxf ncurses-6.2.tar.gz
cd ncurses-6.2
sed -i '/LIBTOOL_INSTALL/d' c++/Makefile.in
./configure --prefix=/usr           \
            --mandir=/usr/share/man \
            --with-shared           \
            --without-debug         \
            --without-normal        \
            --enable-pc-files       \
            --enable-widec
make -j $PARALLEL_JOBS
make install
mv -v /usr/lib/libncursesw.so.6* /lib
ln -sfv ../../lib/$(readlink /usr/lib/libncursesw.so) /usr/lib/libncursesw.so
for lib in ncurses form panel menu ; do
    rm -vf                    /usr/lib/lib${lib}.so
    echo "INPUT(-l${lib}w)" > /usr/lib/lib${lib}.so
    ln -sfv ${lib}w.pc        /usr/lib/pkgconfig/${lib}.pc
done
rm -vf                     /usr/lib/libcursesw.so
echo "INPUT(-lncursesw)" > /usr/lib/libcursesw.so
ln -sfv libncurses.so      /usr/lib/libcurses.so
if [[ $INSTALL_OPTIONAL_DOCS = 1 ]] ; then
    mkdir -v       /usr/share/doc/ncurses-6.2
    cp -v -R doc/* /usr/share/doc/ncurses-6.2
fi
cd /sources
rm -rf ncurses-6.2

echo "# 8.29. Sed-4.8"
tar -Jxf sed-4.8.tar.xz
cd sed-4.8
./configure --prefix=/usr --bindir=/bin
make -j $PARALLEL_JOBS
make install
if [[ $INSTALL_OPTIONAL_DOCS = 1 ]] ; then
    make html
    install -d -m755           /usr/share/doc/sed-4.8
    install -m644 doc/sed.html /usr/share/doc/sed-4.8
fi
cd /sources
rm -rf sed-4.8

echo "# 8.30. Psmisc-23.3"
tar -Jxf psmisc-23.3.tar.xz
cd psmisc-23.3
./configure --prefix=/usr
make -j $PARALLEL_JOBS
make install
mv -v /usr/bin/fuser   /bin
mv -v /usr/bin/killall /bin
cd /sources
rm -rf psmisc-23.3

echo "# 8.31. Gettext-0.21"
tar -Jxf gettext-0.21.tar.xz
cd gettext-0.21
./configure --prefix=/usr    \
            --disable-static \
            --docdir=/usr/share/doc/gettext-0.21
make -j $PARALLEL_JOBS
make install
chmod -v 0755 /usr/lib/preloadable_libintl.so
cd /sources
rm -rf gettext-0.21

echo "# 8.32. Bison-3.7.4"
tar -Jxf bison-3.7.4.tar.xz
cd bison-3.7.4
./configure --prefix=/usr --docdir=/usr/share/doc/bison-3.7.4
make -j $PARALLEL_JOBS
make install
cd /sources
rm -rf bison-3.7.4

echo "# 8.33. Grep-3.6"
tar -Jxf grep-3.6.tar.xz
cd grep-3.6
./configure --prefix=/usr --bindir=/bin
make -j $PARALLEL_JOBS
make install
cd /sources
rm -rf grep-3.6

echo "# 8.34. Bash-5.0"
tar -zxf bash-5.0.tar.gz
cd bash-5.0
patch -Np1 -i ../bash-5.0-upstream_fixes-1.patch
./configure --prefix=/usr                    \
            --docdir=/usr/share/doc/bash-5.0 \
            --without-bash-malloc            \
            --with-installed-readline
make -j $PARALLEL_JOBS
make install
mv -vf /usr/bin/bash /bin
# exec /bin/bash --login +h
# Don't know of a good way to keep running the script after entering bash here.
cd /sources
rm -rf bash-5.0

echo "# 8.35. Libtool-2.4.6"
tar -Jxf libtool-2.4.6.tar.xz
cd libtool-2.4.6
./configure --prefix=/usr
make -j $PARALLEL_JOBS
make install
cd /sources
rm -rf libtool-2.4.6

echo "# 8.36. GDBM-1.18.1"
tar -zxf gdbm-1.18.1.tar.gz
cd gdbm-1.18.1
sed -r -i '/^char.*parseopt_program_(doc|args)/d' src/parseopt.c
./configure --prefix=/usr \
            --disable-static \
            --enable-libgdbm-compat
make -j $PARALLEL_JOBS
make install
cd /sources
rm -rf gdbm-1.18.1

echo "# 8.37. Gperf-3.1"
tar -zxf gperf-3.1.tar.gz
cd gperf-3.1
./configure --prefix=/usr --docdir=/usr/share/doc/gperf-3.1
make -j $PARALLEL_JOBS
make install
cd /sources
rm -rf gperf-3.1

echo "# 8.38. Expat-2.2.10"
tar -Jxf expat-2.2.10.tar.xz
cd expat-2.2.10
./configure --prefix=/usr    \
            --disable-static \
            --docdir=/usr/share/doc/expat-2.2.10
make -j $PARALLEL_JOBS
make install
if [[ $INSTALL_OPTIONAL_DOCS = 1 ]] ; then
    install -v -m644 doc/*.{html,png,css} /usr/share/doc/expat-2.2.10
fi
cd /sources
rm -rf expat-2.2.10

echo "# 8.39. Inetutils-1.9.4"
tar -Jxf inetutils-1.9.4.tar.xz
cd inetutils-1.9.4
./configure --prefix=/usr        \
            --localstatedir=/var \
            --disable-logger     \
            --disable-whois      \
            --disable-rcp        \
            --disable-rexec      \
            --disable-rlogin     \
            --disable-rsh        \
            --disable-servers
make -j $PARALLEL_JOBS
make install
mv -v /usr/bin/{hostname,ping,ping6,traceroute} /bin
mv -v /usr/bin/ifconfig /sbin
cd /sources
rm -rf inetutils-1.9.4

echo "# 8.40. Perl-5.32.0"
tar -Jxf perl-5.32.0.tar.xz
cd perl-5.32.0
export BUILD_ZLIB=False
export BUILD_BZIP2=0
sh Configure -des                                         \
             -Dprefix=/usr                                \
             -Dvendorprefix=/usr                          \
             -Dprivlib=/usr/lib/perl5/5.32/core_perl      \
             -Darchlib=/usr/lib/perl5/5.32/core_perl      \
             -Dsitelib=/usr/lib/perl5/5.32/site_perl      \
             -Dsitearch=/usr/lib/perl5/5.32/site_perl     \
             -Dvendorlib=/usr/lib/perl5/5.32/vendor_perl  \
             -Dvendorarch=/usr/lib/perl5/5.32/vendor_perl \
             -Dman1dir=/usr/share/man/man1                \
             -Dman3dir=/usr/share/man/man3                \
             -Dpager="/usr/bin/less -isR"                 \
             -Duseshrplib                                 \
             -Dusethreads
make -j $PARALLEL_JOBS
make install
unset BUILD_ZLIB BUILD_BZIP2
cd /sources
rm -rf perl-5.32.0

echo "# 8.41. XML::Parser-2.46"
tar -zxf XML-Parser-2.46.tar.gz
cd XML-Parser-2.46
perl Makefile.PL
make -j $PARALLEL_JOBS
make install
cd /sources
rm -rf XML-Parser-2.46

echo "# 8.42. Intltool-0.51.0"
tar -zxf intltool-0.51.0.tar.gz
cd intltool-0.51.0
sed -i 's:\\\${:\\\$\\{:' intltool-update.in
./configure --prefix=/usr
make -j $PARALLEL_JOBS
make install
if [[ $INSTALL_OPTIONAL_DOCS = 1 ]] ; then
    install -v -Dm644 doc/I18N-HOWTO /usr/share/doc/intltool-0.51.0/I18N-HOWTO
fi
cd /sources
rm -rf intltool-0.51.0

echo "# 8.43. Autoconf-2.69"
tar -Jxf autoconf-2.69.tar.xz
cd autoconf-2.69
patch -Np1 -i ../autoconf-2.69-consolidated_fixes-1.patch
./configure --prefix=/usr
make -j $PARALLEL_JOBS
make install
cd /sources
rm -rf autoconf-2.69

echo "# 8.44. Automake-1.16.3"
tar -Jxf automake-1.16.3.tar.xz
cd automake-1.16.3
./configure --prefix=/usr --docdir=/usr/share/doc/automake-1.16.3
make -j $PARALLEL_JOBS
make install
cd /sources
rm -rf automake-1.16.3

echo "# 8.45. kmod-27"
tar -Jxf kmod-27.tar.xz
cd kmod-27
./configure --prefix=/usr          \
            --bindir=/bin          \
            --sysconfdir=/etc      \
            --with-rootlibdir=/lib \
            --with-xz              \
            --with-zlib
make -j $PARALLEL_JOBS
make install
for target in depmod insmod lsmod modinfo modprobe rmmod; do
  ln -sfv ../bin/kmod /sbin/$target
done
ln -sfv kmod /bin/lsmod
cd /sources
rm -rf kmod-27

echo "8.46. Libelf from Elfutils-0.182"
tar -jxf elfutils-0.182.tar.bz2
cd elfutils-0.182
./configure --prefix=/usr                \
            --disable-debuginfod         \
            --enable-libdebuginfod=dummy \
            --libdir=/lib
make -j $PARALLEL_JOBS
make -C libelf install
install -vm644 config/libelf.pc /usr/lib/pkgconfig
rm /lib/libelf.a
cd /sources
rm -rf elfutils-0.182

echo "# 8.47. libffi-3.3"
tar -zxf libffi-3.3.tar.gz
cd libffi-3.3
./configure --prefix=/usr --disable-static --with-gcc-arch=native
make -j $PARALLEL_JOBS
make install 
cd /sources
rm -rf libffi-3.3

echo "# 8.48. OpenSSL-1.1.1h"
tar -zxf openssl-1.1.1h.tar.gz
cd openssl-1.1.1h
./config --prefix=/usr         \
         --openssldir=/etc/ssl \
         --libdir=lib          \
         shared                \
         zlib-dynamic
make -j $PARALLEL_JOBS
sed -i '/INSTALL_LIBS/s/libcrypto.a libssl.a//' Makefile
make MANSUFFIX=ssl install
if [[ $INSTALL_OPTIONAL_DOCS = 1 ]] ; then
    mv -v /usr/share/doc/openssl /usr/share/doc/openssl-1.1.1h
    cp -vfr doc/* /usr/share/doc/openssl-1.1.1h
fi
cd /sources
rm -rf openssl-1.1.1h

echo "# 8.49. Python-3.9.0"
tar -Jxf Python-3.9.0.tar.xz
cd Python-3.9.0
./configure --prefix=/usr       \
            --enable-shared     \
            --with-system-expat \
            --with-system-ffi   \
            --with-ensurepip=yes
make -j $PARALLEL_JOBS
make install 
chmod -v 755 /usr/lib/libpython3.9.so
chmod -v 755 /usr/lib/libpython3.so
ln -sfv pip3.9 /usr/bin/pip3
ln -sfv python3.9 /usr/bin/python
if [[ $INSTALL_OPTIONAL_DOCS = 1 ]] ; then
    install -v -dm755 /usr/share/doc/python-3.9.0/html
    tar --strip-components=1 --no-same-owner --no-same-permissions -C /usr/share/doc/python-3.9.0/html -jxf ../python-3.9.0-docs-html.tar.bz2
fi
cd /sources
rm -rf Python-3.9.0

echo "# 8.50. Ninja-1.10.2"
tar -zxf ninja-1.10.2.tar.gz
cd ninja-1.10.2
sed -i '/int Guess/a \
  int   j = 0;\
  char* jobs = getenv( "NINJAJOBS" );\
  if ( jobs != NULL ) j = atoi( jobs );\
  if ( j > 0 ) return j;\
' src/ninja.cc
python3 configure.py --bootstrap
install -vm755 ninja /usr/bin/
install -vDm644 misc/bash-completion /usr/share/bash-completion/completions/ninja
install -vDm644 misc/zsh-completion /usr/share/zsh/site-functions/_ninja
cd /sources
rm -rf ninja-1.10.2

echo "# 8.51. Meson-0.56.0"
tar -zxf meson-0.56.0.tar.gz
cd meson-0.56.0
python3 setup.py build
python3 setup.py install --root=dest
cp -rv dest/* /
cd /sources
rm -rf meson-0.56.0

echo "# 8.52. Coreutils-8.32"
tar -Jxf coreutils-8.32.tar.xz
cd coreutils-8.32
patch -Np1 -i ../coreutils-8.32-i18n-1.patch
sed -i '/test.lock/s/^/#/' gnulib-tests/gnulib.mk
autoreconf -fiv
FORCE_UNSAFE_CONFIGURE=1 ./configure \
            --prefix=/usr            \
            --enable-no-install-program=kill,uptime
make -j $PARALLEL_JOBS
make install
mv -v /usr/bin/{cat,chgrp,chmod,chown,cp,date,dd,df,echo} /bin
mv -v /usr/bin/{false,ln,ls,mkdir,mknod,mv,pwd,rm} /bin
# Found a problem here where the moved mv binary from the line above can't be found by the next line.
# Inserting a sync as a workaround.
sync
mv -v /usr/bin/{rmdir,stty,sync,true,uname} /bin
mv -v /usr/bin/chroot /usr/sbin
mv -v /usr/share/man/man1/chroot.1 /usr/share/man/man8/chroot.8
sed -i s/\"1\"/\"8\"/1 /usr/share/man/man8/chroot.8
mv -v /usr/bin/{head,nice,sleep,touch} /bin
cd /sources
rm -rf coreutils-8.32

echo "# 8.53. Check-0.15.2"
tar -zxf check-0.15.2.tar.gz
cd check-0.15.2
./configure --prefix=/usr --disable-static
make -j $PARALLEL_JOBS
make docdir=/usr/share/doc/check-0.15.2 install
cd /sources
rm -rf check-0.15.2

echo "# 8.54. Diffutils-3.7"
tar -Jxf diffutils-3.7.tar.xz
cd diffutils-3.7
./configure --prefix=/usr
make -j $PARALLEL_JOBS
make install
cd /sources
rm -rf diffutils-3.7

echo "# 8.55. Gawk-5.1.0"
tar -Jxf gawk-5.1.0.tar.xz
cd gawk-5.1.0
sed -i 's/extras//' Makefile.in
./configure --prefix=/usr
make -j $PARALLEL_JOBS
make install
if [[ $INSTALL_OPTIONAL_DOCS = 1 ]] ; then
    mkdir -v /usr/share/doc/gawk-5.1.0
    cp    -v doc/{awkforai.txt,*.{eps,pdf,jpg}} /usr/share/doc/gawk-5.1.0
fi
cd /sources
rm -rf gawk-5.1.0

echo "# 8.56. Findutils-4.7.0"
tar -Jxf findutils-4.7.0.tar.xz
cd findutils-4.7.0
./configure --prefix=/usr --localstatedir=/var/lib/locate
make -j $PARALLEL_JOBS
make install
mv -v /usr/bin/find /bin
sed -i 's/find:=${BINDIR}/find:=\/bin/' /usr/bin/updatedb
cd /sources
rm -rf findutils-4.7.0

echo "# 8.57. Groff-1.22.4"
tar -zxf groff-1.22.4.tar.gz
cd groff-1.22.4
PAGE=$GROFF_PAPER_SIZE ./configure --prefix=/usr
make -j 1
make install
cd /sources
rm -rf groff-1.22.4

# 8.58. GRUB-2.04
# We don't use GRUB on ARM

echo "# 8.59. Less-563"
tar -zxf less-563.tar.gz
cd less-563
./configure --prefix=/usr --sysconfdir=/etc
make -j $PARALLEL_JOBS
make install
cd /sources
rm -rf less-563

echo "# 8.60. Gzip-1.10"
tar -Jxf gzip-1.10.tar.xz
cd gzip-1.10
./configure --prefix=/usr
make -j $PARALLEL_JOBS
make install
mv -v /usr/bin/gzip /bin
cd /sources
rm -rf gzip-1.10

echo "# 8.61. IPRoute2-5.9.0"
tar -Jxf iproute2-5.9.0.tar.xz
cd iproute2-5.9.0
sed -i /ARPD/d Makefile
rm -fv man/man8/arpd.8
sed -i 's/m_ipt.o//' tc/Makefile
make -j $PARALLEL_JOBS
make DOCDIR=/usr/share/doc/iproute2-5.9.0 install
cd /sources
rm -rf iproute2-5.9.0

echo "# 8.62. Kbd-2.3.0"
tar -Jxf kbd-2.3.0.tar.xz
cd kbd-2.3.0
patch -Np1 -i ../kbd-2.3.0-backspace-1.patch
sed -i 's/\(RESIZECONS_PROGS=\)yes/\1no/g' configure
sed -i 's/resizecons.8 //' docs/man/man8/Makefile.in
./configure --prefix=/usr --disable-vlock
make -j $PARALLEL_JOBS
make install
rm -v /usr/lib/libtswrap.{a,la,so*}
if [[ $INSTALL_OPTIONAL_DOCS = 1 ]] ; then
    mkdir -v /usr/share/doc/kbd-2.3.0
    cp -R -v docs/doc/* /usr/share/doc/kbd-2.3.0
fi
cd /sources
rm -rf kbd-2.3.0

echo "# 8.63. Libpipeline-1.5.3"
tar -zxf libpipeline-1.5.3.tar.gz
cd libpipeline-1.5.3
./configure --prefix=/usr
make -j $PARALLEL_JOBS
make install
cd /sources
rm -rf libpipeline-1.5.3

echo "# 8.64. Make-4.3"
tar -zxf make-4.3.tar.gz
cd make-4.3
./configure --prefix=/usr
make -j $PARALLEL_JOBS
make install
cd /sources
rm -rf make-4.3

echo "# 8.65. Patch-2.7.6"
tar -Jxf patch-2.7.6.tar.xz
cd patch-2.7.6
./configure --prefix=/usr
make -j $PARALLEL_JOBS
make install
cd /sources
rm -rf patch-2.7.6

echo "# 8.66. Man-DB-2.9.3"
tar -Jxf man-db-2.9.3.tar.xz
cd man-db-2.9.3
./configure --prefix=/usr                        \
            --docdir=/usr/share/doc/man-db-2.9.3 \
            --sysconfdir=/etc                    \
            --disable-setuid                     \
            --enable-cache-owner=bin             \
            --with-browser=/usr/bin/lynx         \
            --with-vgrind=/usr/bin/vgrind        \
            --with-grap=/usr/bin/grap            \
            --with-systemdtmpfilesdir=           \
            --with-systemdsystemunitdir=
make -j $PARALLEL_JOBS
make install
cd /sources
rm -rf man-db-2.9.3

echo "# 8.67. Tar-1.32"
tar -Jxf tar-1.32.tar.xz
cd tar-1.32
FORCE_UNSAFE_CONFIGURE=1  \
./configure --prefix=/usr \
            --bindir=/bin
make -j $PARALLEL_JOBS
make install
if [[ $INSTALL_OPTIONAL_DOCS = 1 ]] ; then
    make -C doc install-html docdir=/usr/share/doc/tar-1.32
fi
cd /sources
rm -rf tar-1.32

echo "# 8.68. Texinfo-6.7"
tar -Jxf texinfo-6.7.tar.xz
cd texinfo-6.7
./configure --prefix=/usr --disable-static
make -j $PARALLEL_JOBS
make install
cd /sources
rm -rf texinfo-6.7

echo "# 8.69. Vim-8.2.2070"
tar -zxf vim-8.2.2070.tar.gz
cd vim-8.2.2070
echo '#define SYS_VIMRC_FILE "/etc/vimrc"' >> src/feature.h
./configure --prefix=/usr
make -j $PARALLEL_JOBS
make install
ln -sv vim /usr/bin/vi
for L in /usr/share/man/{,*/}man1/vim.1; do
    ln -sv vim.1 $(dirname $L)/vi.1
done
ln -sv ../vim/vim82/doc /usr/share/doc/vim-8.2.2070
cat > /etc/vimrc << "EOF"
" Begin /etc/vimrc

" Ensure defaults are set before customizing settings, not after
source $VIMRUNTIME/defaults.vim
let skip_defaults_vim=1 

set nocompatible
set backspace=2
set mouse=
syntax on
if (&term == "xterm") || (&term == "putty")
  set background=dark
endif

" End /etc/vimrc
EOF
cd /sources
rm -rf vim-8.2.2070

echo "# 8.70. Eudev-3.2.9"
tar -zxf eudev-3.2.9.tar.gz
cd eudev-3.2.9
./configure --prefix=/usr           \
            --bindir=/sbin          \
            --sbindir=/sbin         \
            --libdir=/usr/lib       \
            --sysconfdir=/etc       \
            --libexecdir=/lib       \
            --with-rootprefix=      \
            --with-rootlibdir=/lib  \
            --enable-manpages       \
            --disable-static
make -j $PARALLEL_JOBS
mkdir -pv /lib/udev/rules.d
mkdir -pv /etc/udev/rules.d
make install
tar -Jxf ../udev-lfs-20171102.tar.xz
make -f udev-lfs-20171102/Makefile.lfs install
udevadm hwdb --update
cd /sources
rm -rf eudev-3.2.9

echo "# 8.71. Procps-ng-3.3.16"
tar -Jxf procps-ng-3.3.16.tar.xz
cd procps-ng-3.3.16
./configure --prefix=/usr                            \
            --exec-prefix=                           \
            --libdir=/usr/lib                        \
            --docdir=/usr/share/doc/procps-ng-3.3.16 \
            --disable-static                         \
            --disable-kill
make -j $PARALLEL_JOBS
make install
mv -v /usr/lib/libprocps.so.* /lib
ln -sfv ../../lib/$(readlink /usr/lib/libprocps.so) /usr/lib/libprocps.so
cd /sources
rm -rf procps-ng-3.3.16

echo "# 8.72. Util-linux-2.36.1"
tar -Jxf util-linux-2.36.1.tar.xz
cd util-linux-2.36.1
mkdir -pv /var/lib/hwclock
./configure ADJTIME_PATH=/var/lib/hwclock/adjtime   \
            --docdir=/usr/share/doc/util-linux-2.36.1 \
            --disable-chfn-chsh  \
            --disable-login      \
            --disable-nologin    \
            --disable-su         \
            --disable-setpriv    \
            --disable-runuser    \
            --disable-pylibmount \
            --disable-static     \
            --without-python     \
            --without-systemd    \
            --without-systemdsystemunitdir
make -j $PARALLEL_JOBS
make install
cd /sources
rm -rf util-linux-2.36.1

echo "# 8.73. E2fsprogs-1.45.6"
tar -zxf e2fsprogs-1.45.6.tar.gz
cd e2fsprogs-1.45.6
mkdir -v build
cd build
../configure --prefix=/usr           \
             --bindir=/bin           \
             --with-root-prefix=""   \
             --enable-elf-shlibs     \
             --disable-libblkid      \
             --disable-libuuid       \
             --disable-uuidd         \
             --disable-fsck
make -j $PARALLEL_JOBS
make install
chmod -v u+w /usr/lib/{libcom_err,libe2p,libext2fs,libss}.a
if [[ $INSTALL_OPTIONAL_DOCS = 1 ]] ; then
    gunzip -v /usr/share/info/libext2fs.info.gz
    install-info --dir-file=/usr/share/info/dir /usr/share/info/libext2fs.info
    makeinfo -o      doc/com_err.info ../lib/et/com_err.texinfo
    install -v -m644 doc/com_err.info /usr/share/info
    install-info --dir-file=/usr/share/info/dir /usr/share/info/com_err.info
fi
cd /sources
rm -rf e2fsprogs-1.45.6

echo "# 8.74. Sysklogd-1.5.1"
tar -zxf sysklogd-1.5.1.tar.gz
cd sysklogd-1.5.1
sed -i '/Error loading kernel symbols/{n;n;d}' ksym_mod.c
sed -i 's/union wait/int/' syslogd.c
make -j $PARALLEL_JOBS
make BINDIR=/sbin install
cat > /etc/syslog.conf << "EOF"
# Begin /etc/syslog.conf

auth,authpriv.* -/var/log/auth.log
*.*;auth,authpriv.none -/var/log/sys.log
daemon.* -/var/log/daemon.log
kern.* -/var/log/kern.log
mail.* -/var/log/mail.log
user.* -/var/log/user.log
*.emerg *

# End /etc/syslog.conf
EOF
cd /sources
rm -rf sysklogd-1.5.1

echo "# 8.75. Sysvinit-2.98"
tar -Jxf sysvinit-2.98.tar.xz
cd sysvinit-2.98
patch -Np1 -i ../sysvinit-2.98-consolidated-1.patch
make -j $PARALLEL_JOBS
make install
cd /sources
rm -rf sysvinit-2.98

echo -e "--------------------------------------------------------------------"
echo -e "\nYou made it! Now there are just a few things left to take care of..."
printf 'Total script time: %s\n' $(timer $total_time)
echo -e "\nYou have not set a root password yet. Go ahead, I'll wait here.\n"
passwd root

echo -e "\nNow about the firmware..."
echo "You probably want to copy the supplied Broadcom libraries to /opt/vc?"
select yn in "Yes" "No"; do
    case $yn in
        Yes) tar -zxf master.tar.gz
             cp -rv /sources/firmware-master/hardfp/opt/vc /opt
             echo "/opt/vc/lib" >> /etc/ld.so.conf.d/broadcom.conf
             ldconfig
             if [[ $RPI_MODEL = 4 ]] ; then
                 tar -zxf v2020.09.03-138a1.tar.gz
                 cd rpi-eeprom-2020.09.03-138a1
                 cp -v rpi-eeprom-update-default /etc/default/rpi-eeprom-update
                 cp -v rpi-eeprom-config rpi-eeprom-update /opt/vc/bin
                 mkdir -pv /lib/firmware/raspberrypi
                 cp -rv firmware /lib/firmware/raspberrypi/bootloader
                 ln -sfv /lib/firmware/raspberrypi/bootloader/vl805 /opt/vc/bin/vl805
                 cd /sources
                 rm -rf rpi-eeprom-2020.09.03-138a1
             fi
             break
             ;;
        No) break;;
    esac
done

echo -e "\nIf you're not going to compile your own kernel you probably want to copy the kernel modules from the firmware package to /lib/modules?"
select yn in "Yes" "No"; do
    case $yn in
        Yes) cp -rv /sources/firmware-master/modules /lib; break;;
        No) break;;
    esac
done

echo -e "\nLast question, if you want I can mount the boot partition and overwrite the kernel and bootloader with the one you downloaded?"
select yn in "Yes" "No"; do
    case $yn in
        Yes) mount /dev/mmcblk0p1 /boot && cp -rv /sources/firmware-master/boot / && umount /boot; break;;
        No) break;;
    esac
done

echo -e "\nThere, all done! Now continue reading from \"8.76. About Debugging Symbols\" to make your system bootable."
echo "And don't forget to check out https://intestinate.com/pilfs/beyond.html when you're done with your build!"
